a = True
b = False
print(a)
print(b)

print(a == b)

a = True
b = True
print(a == b)

a = 2
b = 3
print(a == b)

a = 4
b = 4
print(a == b)

a = 5
b = 3
print(a != b)

b = 5
print(a > b)

print(5 > 4)
print(5 < 4)