a = 5
b = 5
if a == b:
    print('Values are the same.')
else:
    print('Values are not the same.')

b = 3
if a > b:
    b = b + 2
else:
    a = a + 2
print(a)
print(b)

password = 'Password123'

inputed_password = input('Enter the password: ')
inputed_password = str(inputed_password)

if inputed_password == password:
    print('Correct password.')
    print('Acess granted.')
else:
    print('Wrong password.')
    print('Acess denied.')